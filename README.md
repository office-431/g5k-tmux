# G5K TMUX

a basic tmux config for g5k

## Install

```sh
curl https://gitlab.inria.fr/office-431/g5k-tmux/-/raw/master/install.sh | sh
```
