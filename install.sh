FOLDER=~/.g5k-tmux
REPO=git@gitlab.inria.fr:office-431/g5k-tmux.git 

git clone $REPO $FOLDER

ln -s $FOLDER/tmux.conf ~/.tmux.conf

FOLDER_OAR_TIME=~/.oar-time
REPO_OAR_TIME=git@gitlab.inria.fr:office-431/oar-time.git

git clone $REPO_OAR_TIME $FOLDER_OAR_TIME

tmux source-file ~/.tmux.conf
